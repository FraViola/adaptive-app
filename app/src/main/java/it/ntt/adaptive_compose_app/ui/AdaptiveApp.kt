@file:Suppress("OPT_IN_IS_NOT_ENABLED")
package it.ntt.adaptive_compose_app.ui
import android.annotation.SuppressLint
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.*
import androidx.compose.material3.windowsizeclass.WindowSizeClass
import androidx.compose.material3.windowsizeclass.WindowWidthSizeClass
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import it.ntt.adaptive_compose_app.ui.components.AppDrawer
import it.ntt.adaptive_compose_app.ui.components.AppNavRail
import it.ntt.adaptive_compose_app.ui.components.BottomNavigation
import it.ntt.adaptive_compose_app.utils.*
import kotlinx.coroutines.launch

@Composable
fun AdaptiveApp(
    windowSizeClass: WindowSizeClass
) {
    val navController = rememberNavController()
    val navigator = remember(navController) {
        Navigator(navController)
    }
    val currentRoute = getCurrentRoute(navController)


    val navigationType: NavigationType
    val contentType: ContentType

    when (windowSizeClass.widthSizeClass) {
        WindowWidthSizeClass.Compact -> {
            navigationType = NavigationType.BOTTOM_NAVIGATION
            contentType = ContentType.LIST_ONLY
        }
        WindowWidthSizeClass.Medium -> {
            navigationType = NavigationType.NAVIGATION_RAIL
            contentType = ContentType.LIST_AND_DETAIL
        }
        WindowWidthSizeClass.Expanded -> {
            navigationType = NavigationType.PERMANENT_NAVIGATION_DRAWER
            contentType = ContentType.LIST_AND_DETAIL
        }
        else -> {
            navigationType = NavigationType.BOTTOM_NAVIGATION
            contentType = ContentType.LIST_ONLY
        }
    }
    NavGraph(navController = navController, contentType, navigationType, windowSizeClass.widthSizeClass)
    NavigationWrapperUI(
        navigationType = navigationType,
        navigator,
        currentRoute,
        navController
    )

}


@Composable
fun NavigationWrapperUI(
    navigationType: NavigationType,
    navigator: Navigator,
    currentRoute: String,
    navController: NavHostController
) {
    Row(modifier = Modifier.fillMaxSize()) {
        AnimatedVisibility(visible = navigationType == NavigationType.NAVIGATION_RAIL || navigationType == NavigationType.PERMANENT_NAVIGATION_DRAWER) {
            ExpandedView(navigator = navigator, currentRoute = currentRoute)
        }
        Column(
            modifier = Modifier
                .fillMaxSize()
                .background(MaterialTheme.colorScheme.inverseOnSurface)
        ) {
            AnimatedVisibility(visible = navigationType == NavigationType.BOTTOM_NAVIGATION) {
                CompatView(
                    navigator = navigator,
                    currentRoute = currentRoute,
                    navController
                )
            }
        }
    }
}

@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CompatView(
    navigator: Navigator,
    currentRoute: String,
    navController: NavHostController
) {
    Scaffold(bottomBar = {
        BottomNavigation(
            homeAction = { navigator.navigateToHome() },
            displayFeaturesAction = { navigator.navigateToDisplayFeature() },
            adaptiveContentAction = { navigator.navigateToContent() },
            currentRoute = currentRoute
        )
    }) {
        NavGraph(navController = navController, ContentType.LIST_ONLY, NavigationType.BOTTOM_NAVIGATION, WindowWidthSizeClass.Compact)
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ExpandedView(navigator: Navigator, currentRoute: String) {
    val drawerState = rememberDrawerState(DrawerValue.Closed)
    val scope = rememberCoroutineScope()
    ModalNavigationDrawer(
        drawerState = drawerState,
        gesturesEnabled = false,
        content = {
            AppNavRail(
                homeAction = { navigator.navigateToHome() },
                displayFeaturesAction = { navigator.navigateToDisplayFeature() },
                adaptiveContentAction = { navigator.navigateToContent() },
                expandedAction = { scope.launch { drawerState.open() } },
                currentRoute = currentRoute
            )

        },
        drawerContent = {
            AppDrawer(
                homeAction = { navigator.navigateToHome() },
                displayFeaturesAction = { navigator.navigateToDisplayFeature() },
                adaptiveContentAction = { navigator.navigateToContent() },
                clickDrawer = { scope.launch { drawerState.close() } },
                currentRoute = currentRoute,
            )
        }
    )
}