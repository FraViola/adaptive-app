package it.ntt.adaptive_compose_app.utils

import androidx.compose.material3.windowsizeclass.WindowWidthSizeClass
import androidx.compose.runtime.Composable
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import it.ntt.adaptive_compose_app.MainViewModel
import it.ntt.adaptive_compose_app.ui.adaptivecontent.AdaptiveContent
import it.ntt.adaptive_compose_app.ui.displayfeatures.DisplayFeatures
import it.ntt.adaptive_compose_app.ui.home.Home

@Composable
fun NavGraph(navController: NavHostController, contentType: ContentType, navigationType: NavigationType, windowWidthSizeClass: WindowWidthSizeClass) {

    val viewModel: MainViewModel = viewModel()

    NavHost(navController = navController, startDestination = Screen.Home.route) {
        composable(Screen.Home.route) { Home() }
        composable(Screen.DisplayFeature.route) { DisplayFeatures(viewModel, contentType, windowWidthSizeClass, navigationType) }
        composable(Screen.AdaptiveContent.route){ AdaptiveContent(viewModel, contentType) }
    }
}