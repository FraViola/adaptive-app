@file:Suppress("OPT_IN_IS_NOT_ENABLED")

package it.ntt.adaptive_compose_app.ui.components

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import it.ntt.adaptive_compose_app.R
import it.ntt.adaptive_compose_app.utils.Screen

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AppDrawer(
    modifier: Modifier = Modifier,
    homeAction: () -> Unit = {},
    displayFeaturesAction: () -> Unit = {},
    adaptiveContentAction: () -> Unit = {},
    clickDrawer: () -> Unit = {},
    currentRoute: String
) {
    val items = listOf(
        Screen.Home,
        Screen.DisplayFeature,
        Screen.AdaptiveContent,
    )
    ModalDrawerSheet(modifier) {
        Logo(
            modifier = Modifier.padding(horizontal = 28.dp, vertical = 24.dp).clickable {
               clickDrawer()
            }
        )
        items.forEach { item ->
            when (item) {
                Screen.Home -> {
                    NavigationDrawerItem(
                        label = { Text(item.route) },
                        icon = { Icon(item.imageVector, contentDescription = "") },
                        selected = currentRoute == item.route,
                        onClick = { homeAction() },
                    )
                }
                Screen.DisplayFeature -> {
                    NavigationDrawerItem(
                        label = { Text(item.route) },
                        icon = { Icon(item.imageVector, contentDescription = "") },
                        selected = currentRoute == item.route,
                        onClick = { displayFeaturesAction() },
                    )
                }
                Screen.AdaptiveContent -> {
                    NavigationDrawerItem(
                        label = { Text(item.route) },
                        icon = { Icon(item.imageVector, contentDescription = "") },
                        selected = currentRoute == item.route,
                        onClick = {  adaptiveContentAction() },
                    )
                }
            }
        }
    }
}

@Composable
private fun Logo(modifier: Modifier = Modifier) {
    Row(modifier = modifier) {
        Icon(
            painterResource(R.drawable.table),
            contentDescription = null,
            tint = MaterialTheme.colorScheme.primary,
        )
        Spacer(Modifier.width(8.dp))
    }
}
