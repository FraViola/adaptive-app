package it.ntt.adaptive_compose_app

import androidx.compose.material3.windowsizeclass.WindowSizeClass
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.window.layout.DisplayFeature
import androidx.window.layout.WindowMetrics
import it.ntt.adaptive_compose_app.data.Email
import it.ntt.adaptive_compose_app.data.EmailsRepository
import it.ntt.adaptive_compose_app.data.EmailsRepositoryImpl
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

class MainViewModel(private val emailsRepository: EmailsRepository = EmailsRepositoryImpl()): ViewModel() {

    private val _uiState = MutableStateFlow(HomeUIState(loading = true))
    val uiState: StateFlow<HomeUIState> = _uiState

    init {
        observeEmails()
    }

    private fun observeEmails() {
        viewModelScope.launch {
            emailsRepository.getAllEmails()
                .catch { ex ->
                    _uiState.value = HomeUIState(error = ex.message)
                }
                .collect { emails ->
                    _uiState.value = HomeUIState(emails = emails)
                }
        }
    }

    fun interactedWithWinInfo(winState: WinState){
        _uiState.update {
            it.copy(winState = winState)
        }
    }
}

data class HomeUIState(
    val emails : List<Email> = emptyList(),
    val loading: Boolean = false,
    val error: String? = null,
    val winState: WinState? = null
)

data class WinState(
    val currentSpace: WindowMetrics?,
    val maximumSpace: WindowMetrics?,
    val windowSizeClass: WindowSizeClass?,
    val displayFeature: List<DisplayFeature>?
)