package it.ntt.adaptive_compose_app.data

import kotlinx.coroutines.flow.Flow

interface EmailsRepository {
    fun getAllEmails(): Flow<List<Email>>
}