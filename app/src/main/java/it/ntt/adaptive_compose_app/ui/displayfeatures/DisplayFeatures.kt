package it.ntt.adaptive_compose_app.ui.displayfeatures

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.windowsizeclass.WindowWidthSizeClass
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.window.layout.FoldingFeature
import it.ntt.adaptive_compose_app.MainViewModel
import it.ntt.adaptive_compose_app.R
import it.ntt.adaptive_compose_app.utils.ContentType
import it.ntt.adaptive_compose_app.utils.NavigationType
import it.ntt.adaptive_compose_app.utils.isBookPosture
import it.ntt.adaptive_compose_app.utils.isSeparating

@Composable
fun DisplayFeatures(
    viewModel: MainViewModel,
    contentType: ContentType,
    windowWidthSizeClass: WindowWidthSizeClass,
    navigationType: NavigationType
) {
    val uiState by viewModel.uiState.collectAsState()

    Column(
        modifier = if (windowWidthSizeClass != WindowWidthSizeClass.Compact) Modifier.padding(
            start = 80.dp
        ) else Modifier
    ) {
        MessageCard(
            msg = Message(
                "Maximum space:",
                uiState.winState?.maximumSpace?.bounds?.flattenToString().orEmpty()
            )
        )
        MessageCard(
            msg = Message(
                "Current space:",
                uiState.winState?.maximumSpace?.bounds?.flattenToString().orEmpty()
            )
        )
        MessageCard(
            msg = Message(
                "WithSizeClass:",
                windowWidthSizeClass.toString().split(".")[1].uppercase()
            )
        )
        MessageCard(
            msg = Message(
                "ContentType:",
                contentType.toString()
            )
        )
        MessageCard(
            msg = Message(
                "NavigationType:",
                navigationType.toString()
            )
        )
        MessageCard(
            msg = Message(
                "Is Book Posture", isBookPosture(
                    uiState.winState?.displayFeature?.filterIsInstance<FoldingFeature>()
                        ?.firstOrNull()
                ).toString()
            )
        )
        MessageCard(
            msg = Message(
                "Is Separating", isSeparating(
                    uiState.winState?.displayFeature?.filterIsInstance<FoldingFeature>()
                        ?.firstOrNull()
                ).toString()
            )
        )
        MessageCard(
            msg = Message(
                "State",
                (uiState.winState?.displayFeature?.filterIsInstance<FoldingFeature>()
                    ?.firstOrNull()?.state ?: if (windowWidthSizeClass == WindowWidthSizeClass.Compact) FoldingFeature.State.HALF_OPENED else FoldingFeature.State.FLAT).toString()
            )
        )

        MessageCard(
            msg = Message(
                "Orientation",
                (uiState.winState?.displayFeature?.filterIsInstance<FoldingFeature>()
                    ?.firstOrNull()?.orientation ?: "ND").toString()
            )
        )

        MessageCard(
            msg = Message(
                "Occlusion type:",
                (uiState.winState?.displayFeature?.filterIsInstance<FoldingFeature>()
                    ?.firstOrNull()?.occlusionType ?: FoldingFeature.OcclusionType.NONE).toString()
            )
        )

    }
}

@Composable
fun MessageCard(msg: Message) {
    Row(modifier = Modifier.padding(all = 8.dp)) {
        Image(
            painter = painterResource(R.drawable.table),
            contentDescription = "Contact profile picture",
            modifier = Modifier
                .size(40.dp)
                .clip(CircleShape)
        )

        Spacer(modifier = Modifier.width(8.dp))

        Column {
            Text(text = msg.feature, style = MaterialTheme.typography.titleMedium)
            Spacer(modifier = Modifier.height(4.dp))
            Text(text = msg.value)
        }
    }
}

data class Message(
    val feature: String,
    val value: String
)