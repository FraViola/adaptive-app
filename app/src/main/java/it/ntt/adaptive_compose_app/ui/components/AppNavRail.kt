package it.ntt.adaptive_compose_app.ui.components

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.NavigationRail
import androidx.compose.material3.NavigationRailItem
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import it.ntt.adaptive_compose_app.R
import it.ntt.adaptive_compose_app.utils.Screen

@Composable
fun AppNavRail(
    modifier: Modifier = Modifier,
    homeAction: () -> Unit = {},
    displayFeaturesAction: () -> Unit = {},
    adaptiveContentAction: () -> Unit = {},
    expandedAction: () -> Unit = {},
    currentRoute: String
) {
    val items = listOf(
        Screen.Home,
        Screen.DisplayFeature,
        Screen.AdaptiveContent,
    )
    Row(Modifier.clickable {
        expandedAction()
    }) {
        NavigationRail(
            header = {
                Icon(
                    painterResource(R.drawable.table),
                    null,
                    Modifier.padding(vertical = 12.dp),
                    tint = MaterialTheme.colorScheme.primary
                )
            },
            modifier = modifier
        ) {
            items.forEach { item ->
                when (item) {
                    Screen.Home -> {
                        NavigationRailItem(
                            label = { Text(item.route) },
                            icon = { Icon(item.imageVector, contentDescription = "") },
                            selected = currentRoute == item.route,
                            onClick = {homeAction() },
                            alwaysShowLabel = false
                        )
                    }
                    Screen.DisplayFeature -> {
                        NavigationRailItem(
                            label = { Text(item.route) },
                            icon = { Icon(item.imageVector, contentDescription = "") },
                            selected = currentRoute == item.route,
                            onClick = { displayFeaturesAction() },
                            alwaysShowLabel = false
                        )
                    }
                    Screen.AdaptiveContent -> {
                        NavigationRailItem(
                            label = { Text(item.route) },
                            icon = { Icon(item.imageVector, contentDescription = "") },
                            selected = currentRoute == item.route,
                            onClick = { adaptiveContentAction() },
                            alwaysShowLabel = false
                        )
                    }
                }
            }

        }
    }
}