package it.ntt.adaptive_compose_app.ui.adaptivecontent

import androidx.compose.foundation.layout.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.microsoft.device.dualscreen.twopanelayout.TwoPaneLayout
import it.ntt.adaptive_compose_app.MainViewModel
import it.ntt.adaptive_compose_app.utils.ContentType

@Composable
fun AdaptiveContent(viewModel: MainViewModel, contentType: ContentType) {
    val uiState by viewModel.uiState.collectAsState()

    if (contentType == ContentType.LIST_ONLY) {
        ListOnlyContent(HomeUIState = uiState, {

        })
    } else {
        TwoPaneLayout(
            pane1 = { Row(Modifier.padding(start = 80.dp)) {
                ListOnlyContent(HomeUIState = uiState, {

                }) }
            },
            pane2 = { EmailThreadItem(email = uiState.emails[1], modifier = Modifier) }
        )
    }
}