package it.ntt.adaptive_compose_app.data

enum class MailboxType {
    INBOX, DRAFTS, SENT, SPAM, TRASH
}