package it.ntt.adaptive_compose_app.ui.components

import androidx.compose.material.BottomNavigationItem
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import it.ntt.adaptive_compose_app.utils.Screen

@Composable
fun BottomNavigation(
    modifier: Modifier = Modifier,
    homeAction: () -> Unit = {},
    displayFeaturesAction: () -> Unit = {},
    adaptiveContentAction: () -> Unit = {},
    currentRoute: String
) {
    val items = listOf(
        Screen.Home,
        Screen.DisplayFeature,
        Screen.AdaptiveContent,
    )
    androidx.compose.material.BottomNavigation(modifier = modifier) {
        items.forEach { item ->
            when (item) {
                Screen.Home -> {
                    BottomNavigationItem(
                        label = { Text(item.route) },
                        icon = { Icon(item.imageVector, contentDescription = "") },
                        selected = currentRoute == item.route,
                        onClick = { homeAction() },
                        alwaysShowLabel = false
                    )
                }
                Screen.DisplayFeature -> {
                    BottomNavigationItem(
                        label = { Text(item.route) },
                        icon = { Icon(item.imageVector, contentDescription = "") },
                        selected = currentRoute == item.route,
                        onClick = { displayFeaturesAction() },
                        alwaysShowLabel = false
                    )
                }
                Screen.AdaptiveContent -> {
                    BottomNavigationItem(
                        label = { Text(item.route) },
                        icon = { Icon(item.imageVector, contentDescription = "") },
                        selected = currentRoute == item.route,
                        onClick = { adaptiveContentAction() },
                        alwaysShowLabel = false
                    )
                }
            }
        }
    }
}