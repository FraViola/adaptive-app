package it.ntt.adaptive_compose_app.utils

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.Phone
import androidx.compose.material.icons.filled.Settings
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.NavHostController

sealed class Screen(val route: String, val imageVector: ImageVector) {
    object Home : Screen("Home", Icons.Filled.Home)
    object DisplayFeature : Screen("DisplayFeature", Icons.Filled.Phone)
    object AdaptiveContent: Screen("Content", Icons.Filled.Settings)
}

class Navigator(navController: NavHostController) {
    val navigateToHome: () -> Unit = {
        navController.navigate(Screen.Home.route) {
            popUpTo(navController.graph.findStartDestination().id) {
                saveState = true
            }
            launchSingleTop = true
            restoreState = true
        }
    }
    val navigateToDisplayFeature: () -> Unit = {
        navController.navigate(Screen.DisplayFeature.route) {
            popUpTo(navController.graph.findStartDestination().id) {
                saveState = true
            }
            launchSingleTop = true
            restoreState = true
        }
    }

    val navigateToContent: () -> Unit = {
        navController.navigate(Screen.AdaptiveContent.route) {
            popUpTo(navController.graph.findStartDestination().id) {
                saveState = true
            }
            launchSingleTop = true
            restoreState = true
        }
    }
}