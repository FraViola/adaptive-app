package it.ntt.adaptive_compose_app.ui.home

import androidx.compose.foundation.layout.Row
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import it.ntt.adaptive_compose_app.utils.Screen

@Composable
fun Home(){
    Row {
       Text(text = Screen.Home.route)
    }
}