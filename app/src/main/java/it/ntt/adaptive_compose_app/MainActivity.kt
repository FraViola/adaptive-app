package it.ntt.adaptive_compose_app

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.*
import androidx.compose.material3.*
import androidx.compose.material3.windowsizeclass.ExperimentalMaterial3WindowSizeClassApi
import androidx.compose.material3.windowsizeclass.calculateWindowSizeClass
import androidx.window.layout.WindowMetricsCalculator
import com.google.accompanist.adaptive.calculateDisplayFeatures
import it.ntt.adaptive_compose_app.ui.AdaptiveApp

class MainActivity : ComponentActivity() {

    private val viewModel: MainViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        setContent {
            val wm = WindowMetricsCalculator.getOrCreate()
            val currentSpace = wm.computeCurrentWindowMetrics(this)
            val maximumSpace = wm.computeMaximumWindowMetrics(this)

            /**
             * @see calculateWindowSizeClass function of androidx.compose.material3 is experimental,
             * but the windowsManger equivalent 'WindowMetricsCalculator' can be used.
             * Using the windowsManager function, parse from dimensions to enums of type: compat, medium or expanded.
             * doc: https://developer.android.com/guide/topics/large-screens/support-different-screen-sizes#window_size_classes
             */
            @Suppress("OPT_IN_IS_NOT_ENABLED")
            @OptIn(ExperimentalMaterial3WindowSizeClassApi::class)
            val windowSizeClass = calculateWindowSizeClass(this)

            val displayFeature = calculateDisplayFeatures(this)

            viewModel.interactedWithWinInfo(
                WinState(
                    currentSpace = currentSpace,
                    maximumSpace = maximumSpace,
                    windowSizeClass = windowSizeClass,
                    displayFeature = displayFeature
                )
            )

            AdaptiveApp(
                windowSizeClass = windowSizeClass
            )
        }
    }
}